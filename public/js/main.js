$(document).ready(function () {
  // SELECT // $('.select2').select2({
  //   minimumResultsForSearch: -1
  // });э
  $('.telephone').mask('+77999999999');
  $('.parent-container').magnificPopup({
    delegate: 'a',
    type: 'image',
    mainClass: 'mfp-with-zoom',
    gallery: {
      enabled: true
    },
    zoom: {
      enabled: true,
      duration: 300,
      easing: 'ease-in-out',
      opener: function (openerElement) {
        return openerElement.is('img') ? openerElement : openerElement.find('img');
      }
    }
  });

  $("#cmbIdioma").select2({
    minimumResultsForSearch: Infinity,
    templateResult: function (idioma) {
      var $span = $(`<span><img src="${$(idioma.element).attr('data-image')}" />` + idioma.text + "</span>");
      return $span;
    },
    templateSelection: function (idioma) {
      var $span = $(`<span><img src="${$(idioma.element).attr('data-image')}" />` + idioma.text + "</span>");
      return $span;
    }
  });

  // $('.js-example-basic-single').select2({
  //   minimumResultsForSearch: Infinity
  // });
  // function formatState(state) {
  //   if (!state.id) {
  //     return state.text;
  //   }
  //   var baseUrl = "/user/pages/images/flags";
  //   var $state = $(
  //     '<span><img src="' + baseUrl + '/' + state.element.value.toLowerCase() + '.png" class="img-flag" /> ' + state.text + '</span>'
  //   );
  //   return $state;
  // };

  // $(".js-example-templating").select2({
  //   templateResult: formatState
  // });
  // SELECT
  $('.link-overflow').click(function (e) {
    e.preventDefault();
    $('.service-info').toggleClass('service-info-active');
    console.log('overflow')
  });

  $('.company-item, .anchor').click(function (e) {
    console.log('link')
    let link = $(this).attr('href');
    let top = $(link).offset().top;

    $('body,html').animate({ scrollTop: top }, 1000);
  });

  var hash = location.hash;
  if ($(hash).length) {
    var top = $(hash).offset().top - 50;
    $('body,html').animate({ scrollTop: top }, 1000);
  }
  new WOW().init();



  $('.burger-menu-btn').click(function (e) {
    e.preventDefault();
    $(this).toggleClass('burger-menu-lines-active');
    $('.nav-panel-mobil').toggleClass('nav-panel-mobil-active');
    $('body').toggleClass('body-overflow');
  });
  $('.transport-slider').slick({
    infinite: true,
    autoplay: false,
    slidesToShow: 1,
    adaptiveHeight: true
  });
  // $('.slider-base').slick({
  //   slidesToShow: 1,
  //   slidesToScroll: 1,
  //   fade: true,
  //   asNavFor: '.slider-list'
  // });
  // $('.slider-list').slick({
  //   slidesToShow: 3,
  //   slidesToScroll: 1,
  //   asNavFor: '.slider-base',
  //   centerMode: true,
  //   focusOnSelect: true
  // });





  $('.partners-slider').slick({
    infinite: true,
    slidesToShow: 3,
    slidesToScroll: 3,
    autoplay: true,
    responsive: [
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
    ]
  })
  $('.gallery-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true
  });
  $('#slider-height').css({"height":"auto","opacity": "1"});
  
  $('.company-slider').slick({
    infinite: true,
    slidesToShow: 1,
    slidesToScroll: 1,
    autoplay: true
  });
  $('.clients-company').slick({
    slidesToShow: 6,
    slidesToScroll: 1,
    autoplay: true,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
  $('.certificates-slider').slick({
    infinite: true,
    slidesToShow: 5,
    slidesToScroll: 1,
    autoplay: false,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 3,
          slidesToScroll: 1,
          infinite: true,
          dots: true
        }
      },
      {
        breakpoint: 600,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      },
      {
        breakpoint: 480,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1
        }
      }
    ]
  });
});
let searchInput = document.querySelector('.search-input')
let searchImg = document.querySelector('.search-img')
$(searchImg).hover(() => {
  $(searchInput).css('transform', 'translateX(0)');
});
window.onclick = function (e) {
  if (e.target !== searchInput && e.target !== searchImg) {
    $(searchInput).css('transform', 'translateX(140%)');
  }
}

