<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 06.03.2020
 * Time: 16:35
 */



function cutStr($str, $length=300, $postfix=' ...')
{
    if ( strlen($str) <= $length)
        return $str;

    $temp = substr($str, 0, $length);
    return substr($temp, 0, strrpos($temp, ' ') ) . $postfix;
}
