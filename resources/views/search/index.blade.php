@extends('layouts.app')
@section('content')
<div class="search-page" >
<div class="container" >
   <div class="search-title">
       @if($resultStatus)
           <h1>@lang('texts.Результаты по запросу'):</h1>
           <h2>{{ $text }}</h2>
       @else
           <h1>@lang('texts.По запросу'):</h1>
           <h2>{{ $text }}</h2>
           <h1  style="margin-left: 20px;">@lang('texts.ничего не найдено')</h1>
       @endif
   </div>

   <div class="search-info">
       @if($resultStatus)
           {{--   ABOUT   --}}
            @if($about != null || $whyOurCompany->count() > 0 || $findOutMore != null)
                <a class='search-info-title' href="{{ route('about') }}"><i class="fas fa-arrow-right"></i>@lang('texts.О компании')</a>
           @endif
            @if($about != null)
               <p style="margin-top: 20px;">{!! cutStr($about->first_block_con) !!}</p>
               <a style="margin-bottom: 50px;" href="{{ route('about') }}#about" class="read-continue">@lang('buttons.Читать далее') <i class="fas fa-chevron-down"></i></a>
            @endif
            @foreach($whyOurCompany as $v)
               <p style="margin-top: 20px;">{{ $v->title }}</p>
               <p>{!! cutStr($v->content) !!}</p>
               <a href="{{ route('about') }}#whyOurCompany" class="read-continue">@lang('buttons.Читать далее') <i class="fas fa-chevron-down"></i></a>
            @endforeach
           @if($findOutMore != null)
               <p style="margin-top: 20px;">{!! cutStr($findOutMore->content) !!}</p>
               <a href="{{ route('about') }}#about-review" class="read-continue">@lang('buttons.Читать далее') <i class="fas fa-chevron-down"></i></a>
           @endif

           {{--  END ABOUT  --}}


           {{--   SERVICES   --}}

           @if($service_content->count() > 0 || $services_first->count() > 0 || $services_second->count() > 0 || $services_third->count() > 0)
               @foreach($services as $k => $v)
                   @if(isset($services_first[$k]) || isset($service_content[$k]) || isset($services_second[$k]) || isset($services_third[$k]))
                    <br><br><a  class='search-info-title' href="{{ route('inner_service', ['url' => $v->url]) }}"><i class="fas fa-arrow-right"></i>{{ $v->name }}</a>
                   @endif
                   @if(isset($service_content[$k]))
                       <p style="margin-top: 5px;">{!! cutStr($v->service_content) !!}</p>
                       <a  href="{{ route('all_service') }}#{{ $v->url }}_service" class="read-continue">@lang('buttons.Читать далее') <i class="fas fa-chevron-down"></i></a>
                       <br> <br>
                   @endif
                   @if(isset($services_first[$k]))
                       <p style="margin-top:5px;">{!! cutStr($v->inner_first_block) !!}</p>
                       <a  href="{{ route('inner_service', ['url' => $v->url]) }}#inner_first_block" class="read-continue">@lang('buttons.Читать далее') <i class="fas fa-chevron-down"></i></a>
                       <br> <br>
                   @endif

                   @if(isset($services_second[$k]))
                       <p style="margin-top: 5px;">{!! cutStr($v->inner_second_block) !!}</p>
                       <a  href="{{ route('inner_service', ['url' => $v->url]) }}#inner_second_block" class="read-continue">@lang('buttons.Читать далее') <i class="fas fa-chevron-down"></i></a>
                       <br> <br>
                   @endif

                   @if(isset($services_third[$k]))
                       <p style="margin-top: 5px;">{!! cutStr($v->inner_third_block_content) !!}</p>
                       <a  href="{{ route('inner_service', ['url' => $v->url]) }}#inner_third_block_content" class="read-continue">@lang('buttons.Читать далее') <i class="fas fa-chevron-down"></i></a>
                       <br> <br>
                   @endif
               @endforeach
           @endif

           {{--  END SERVICES  --}}

           {{--  REVIEW  --}}
           @if($reviews->count() > 0)
               <br><br>
               <a class='search-info-title' href="{{ route('reviews') }}"><i class="fas fa-arrow-right"></i>@lang('texts.Отзывы')</a>
               @foreach($reviews as $k => $v)
                   <h2 style="margin-top: 10px;">«{{ $v->company }}» {{ $v->name }}</h2>
                   <p >{!! cutStr($v->content) !!}</p>
                   <a href="{{ $v->url }}" class="read-continue">@lang('buttons.Читать далее') <i class="fas fa-chevron-down"></i></a>
               @endforeach
           @endif
           {{--  END REVIEW  --}}


           {{--  ADDRESS  --}}
           @if($addresss != null)
               <br><br>
               <a class='search-info-title' href="{{ route('contact') }}#contacts"><i class="fas fa-arrow-right"></i>@lang('texts.Адрес')</a>
               <p style="margin-top: 10px;">{!! cutStr($addresss->name) !!}</p>
               <a href="{{ route('contact') }}#contacts" class="read-continue">@lang('buttons.Читать далее') <i class="fas fa-chevron-down"></i></a>
           @endif
           {{--  END ADDRESS  --}}


           {{--  ADDRESS  --}}
           @if($filials->count() > 0)
               <br><br>
               <a class='search-info-title' href="{{ route('contact') }}#filials"><i class="fas fa-arrow-right"></i>@lang('texts.Филиалы компании')</a>
               @foreach($filials as $k => $v)
                   <h1 style="margin-top: 10px;"> {{ $v->country }}</h1>
                   <p>{!! cutStr($v->address) !!}</p>
                   <a href="{{ route('contact') }}#filial_{{ $v->id }}" class="read-continue">@lang('buttons.Читать далее') <i class="fas fa-chevron-down"></i></a>

               @endforeach
           @endif
           {{--  END ADDRESS  --}}
       @endif
   </div>
   </div>
</div>
</div></div></div></div></div></div></div></div></div>


</div>
</div>

</div>
</div>



@endsection
