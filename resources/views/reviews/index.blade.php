@extends('layouts.app')
@section('content')

    @include('/partials/breadcrumbs')

    <div class="review-content">
        <div class="container">
            @foreach($model as $k => $v)
                <div class="review-card" id="review_{{ $v->id }}">
                    <div class="row">
                        <div class="col-xl-2 col-md-3">
                            <div class="review-image">
                                <img src="{{ Voyager::image($v->image) }}" alt="">
                            </div>
                        </div>
                        <div class="col-xl-10 col-md-8">
                            <div class="review-txt">
                                <div class="review-info">
                                    <h2>«{{ $v->company }}» {{ $v->name }}</h2>
                                    <span>{{ date('d/m/Y', strtotime($v->created_at)) }}</span>
                                </div>
                                <p>{{ $v->content }}</p>
                            </div>
                        </div>
                    </div>
                </div>
            @endforeach

            @include('/partials/pagination', compact('model','count'))
        </div>
    </div>
@endsection
