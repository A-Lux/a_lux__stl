@extends('layouts.main')
@section('content')


@if(count($services) == 7)
<section class="main">
    <div class="main-inner wow bounceInUp">
        <div class="container main-item">
            <div class="row cn " style="margin: 0">
                <div class="col-12 col-xs-12 col-sm-12 col-md-4 col-lg-4 p-0">
                    <div class="main-content">

                        @include('/partials/main_partner', ['class' => 'avto', 'service' => $services[0]])
                        @include('/partials/main_partner', ['class' => 'gruz', 'service' => $services[3]])

                    </div>
                </div>


                <div class="col-12 col-xs-12 col-sm-12 col-md-4 col-lg-4  p-0">
                    <div class="main-content">

                        @include('/partials/main_partner', ['class' => 'avia', 'service' => $services[1]])
                        @include('/partials/main_partner', ['class' => 'customs', 'service' => $services[5]])

                    </div>
                </div>
                <div class="col-12 col-xs-12 col-sm-12 col-md-4 col-lg-4 p-0">
                    <div class="main-content">

                        @include('/partials/main_partner', ['class' => 'jd', 'service' => $services[2]])
                        @include('/partials/main_partner', ['class' => 'check', 'service' => $services[6]])
                        @include('/partials/main_partner', ['class' => 'multi-modal', 'service' => $services[4]])

                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
@endif

<form class="counter wow zoomIn" id="counter">
    {{csrf_field()}}

    <h1>{{ count($head) > 0 ? $head[0]->text : "" }}</h1>
    <div class="container">
        <div class="row">
            <div class="col-xl-3 col-md-4 col-lg-3">
                <div>
                    <select id="cmbIdioma" style="width: 200px;" name="service_id">
                        @foreach($services as $k => $v)
                            <option value="{{ $v->id }}" data-image="{{ Voyager::image($v->icon) }}">{{ $v->name }}</option>
                        @endforeach
                    </select>
                </div>
            </div>
            <div class="col-xl-9 col-md-8 col-lg-9 p-0">
                <div class="top-form">
                    <div class="col-xl-4">
                        <div class="input-focus forms ">
                            <input type="text" name="cargo_from" required>
                            <label>@lang('texts.Откуда')*</label>
                        </div>
                    </div>
                    <div class="col-xl-4 ">
                        <div class="input-focus2 forms  ">
                            <input type="text" name="cargo_to" required>
                            <label>@lang('texts.Куда')*</label>
                        </div>
                    </div>
                    <div class="col-xl-4">
                        <div class="input-focus3 forms ">
                            <input type="text" name="description" required>
                            <label>@lang('texts.Описание (вес, объем, размер)')</label>
                        </div>
                    </div>
                </div>
                <div class="bottom-form">
                    <div class="col-xl-6">
                        <div class="input-focus4 forms ">
                            <input type="text" name="telephone" class="telephone" required>
                            <label>@lang('texts.Телефон')*</label>
                        </div>
                    </div>
                    <div class="col-xl-6">
                        <div class="input-focus4  forms">
                            <input type="email" name="email" required>
                            <label>@lang('texts.Ваш email')*</label>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="count-link">
        <div class='counter-link'>
            <button type="button" id="serviceRequestBtn">@lang('buttons.РАСЧИТАТЬ СТОИМОСТЬ')</button>
        </div>
    </div>

</form>

<section class="connection">
    <div class="container">
        <h3>{{ count($head) > 1 ? $head[1]->text : "" }}</h3>
        <div class="row justify-content-start connect">
            @foreach($benefits as $k => $v)
            <div class="wow bounceInLeft col-6 col-xs-{{ $k < 3 ? "12" : "3"}} col-sm-12 col-md-6 col-lg-5 content ">
                <h1>{{ $v->title }}</h1>
                <p>{{ $v->subtitle }}</p>
            </div>
            @endforeach
        </div>
    </div>
</section>
<section class="partners">
    <h1>{{ count($head) > 2 ? $head[2]->text : "" }}</h1>
    <div class="container">
        <div class="row pl">
            @php $m = 0; @endphp
            @foreach($partners as $k => $v)
            @php $m++; @endphp
            @if($m < 13) <div class="col-4 col-xs-2 col-sm-3 col-lg-3">
                <div class="partners-logo wow zoomIn">
                    <img src="{{ Voyager::image($v->image_main) }}">
                </div>
        </div>
        @endif
        @endforeach

    </div>
    </div>


</section>
<section class="partners-mobile">

    <div class="container">
        <h1>{{ count($head) > 2 ? $head[2]->text : "" }}</h1>
        <div class="partners-slider">
            @foreach($partners as $k => $v)
            <div class="partner-slide">
                <img src="{{ Voyager::image($v->image_main) }}">
            </div>
            @endforeach
        </div>
    </div>
</section>

@if(count($partners) > 12)
<div class="container">
    <div class="collapse-link text-center">

        <div class="collapse" id="collapseExample-partners">
            <div class="collapse-content">
                <div class="row">
                    @php $m = 0; @endphp
                    @foreach($partners as $k => $v)
                    @php $m++; @endphp
                    @if($m > 12)
                    <div class="col-4 col-xs-2 col-sm-2 col-lg-2">
                        <div class="partners-logo">
                            <img src="{{ Voyager::image($v->image_main) }}">
                        </div>
                    </div>
                    @endif
                    @endforeach
                </div>
            </div>
        </div>
        <button class="btn collapse-btn" type="button" data-toggle="collapse" data-target="#collapseExample-partners" aria-expanded="false" aria-controls="collapseExample-partners">
            @lang('buttons.Посмотреть еще')
        </button>
    </div>
</div>
@endif


@if(isset($_GET['service']))
    <script>
        document.getElementById("counter").scrollIntoView({ behavior: 'smooth', block: 'end', inline: 'start' });
        $('#cmbIdioma').val({{ $_GET['service'] }});
        $('#cmbIdioma').trigger('change');

    </script>
@endif


@endsection
