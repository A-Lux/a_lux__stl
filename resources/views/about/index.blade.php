@extends('layouts.app')
@section('content')

@include('/partials/breadcrumbs')

<div class="about-video wow bounceInleft">
    <div class="about-page wow bounceInUp" id="about">
        <div class="container">
            <div class="about-content">
                <div class="row">
                    <div class="col-xl-10 col-md-10">
                        <div class="about-text">
                            <p>{!! $model->first_block_con !!} </p>
                        </div>
                    </div>
                    <div class="col-xl-2 col-md-2">
                        <div class="about-image">
                            <img src="{{ Voyager::image($model->first_block_image) }}" alt="">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="container">
        <div class="video-inner">
            <div class="row">
                <div class="col-xl-5">
                    <div class="video-content about-title">
                        <h1>{{ $model->second_block_title }}</h1>
                        <p>{!! $model->second_block_con !!}</p>
                        <div class="text-center">
                            <a href="{{ Voyager::image(json_decode(setting('site.brochure'))[0]->download_link) }}" target="_blank">
                                <button class="head-btn">@lang('buttons.Скачать брошюру')</button></a>
                        </div>
                    </div>
                </div>
                <div class="col-xl-7">
                    <iframe width="650" height="343" src="{{ $model->second_block_url }}" frameborder="0" allow="accelerometer; autoplay; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                </div>
            </div>
        </div>
        <div class="partners-info wow bounceInUp" id="partners-info">
            <div class="partners-info-title">
                <h1>{{ $model->third_block_title }}</h1>
                <p>{{ $model->third_block_date }}</p>
            </div>

            {!! $model->third_block_co !!}
        </div>

    </div>
    <div class="about-us  wow bounceInRight" id="whyOurCompany">
        <div class="container">
            <h1>{{ count($head) > 0 ? $head[0]->name : "" }}</h1>
            <div class="row">
                @foreach($whyOurCompany as $k => $v)
                <div class="col-xl-6">
                    <div class="advantages {{ $k < 2 ? 'advantages-top' : '' }}">
                        <div class="advantages-image">
                            <img src="{{ Voyager::image($v->icon) }}" alt="">
                        </div>
                        <div class="advantages-text">
                            <h1>{{ $v->title }} </h1>
                            <p>{{ $v->content }}</p>
                        </div>
                    </div>
                </div>
                @endforeach

                <div class="col-xl-12">
                    <div class="advantages-btn text-center">
                        <button data-toggle="modal" class="head-btn" data-target="#consultationModal">@lang('buttons.Получить консультацию')</button>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="about-company  wow bounceInLeft">
        <div class="container" id="about-review">
            <div class="row">
                <div class="col-xl-7 col-md-6">
                    <div class="about-company-text">
                        <h1>{{ count($head) > 1 ? $head[1]->name : "" }}</h1>
                        {!! $findOutMore->content !!}
                        <img src="{{ asset("images/arrow.png") }}" alt="">
                    </div>
                </div>
                <div class="col-xl-5 p-0 col-md-6 " id="slider-height" style="height: 700px;opacity:0;">
                    <div class="about-company-slider">
                        <div class="company-slider">
                            @foreach($reviews as $k => $v)
                            <div class="stl-slide">
                                <div>
                                    <div class='stl-slide-content'>
                                        <img src="{{ Voyager::image($v->image) }}" alt="">
                                        <div class="stl-slide-text">
                                            <h1>{{ $v->company }}</h1>
                                        </div>
                                    </div>
                                    <div class="stl-slide-text">
                                        <p>{{ $v->name }} </p>
                                    </div>
                                    <p>{{ $v->content }}</p>
                                </div>
                            </div>
                            @endforeach
                        </div>
                        <div class="stl-slide-btn">
                            <a href="{{ route('reviews') }}">
                                <button class="head-btn">@lang('buttons.Больше отзывов о нас')</button>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="clientsDiv" id="clients-link" style="height: 600px">
        <div class="container">
            <div class="about-title">
                <h1>{{ count($head) > 2 ? $head[2]->name : "" }}</h1>
            </div>
            <div class="clients-company">
                @php $m = 0 @endphp
                @foreach($partners as $k => $v)
                @php $m++ @endphp
                @if($m % 2 == 1)
                <div class="clients-head">
                    @endif
                    <div class="clients-img wow zoomInUp">
                        <img src='{{ Voyager::image($v->image) }}'>
                    </div>
                    @if($m % 2 == 0)
                    </div>
                    @endif
                @endforeach
                @if($m % 2 != 0)
                </div>
                @endif
            </div>
        </div>
    </div>

    <div class="certificates">
        <div class="container">
            <div class="about-title certificate-title">
                <h1>{{ count($head) > 3 ? $head[3]->name : "" }}</h1>
            </div>
            <div class="certificates-slider">
                @foreach($certificates as $k => $v)
                <div class="certificate parent-container wow bounceInUp">
                    <a href="{{ Voyager::image($v->image) }}">
                        <img src="{{ Voyager::image($v->image) }}" alt="">
                    </a>
                </div>
                @endforeach
            </div>
        </div>
    </div>
</div>
@endsection
