<div class='{{ $class }} service' style="background-image: url({{ Voyager::image($service->main_image) }})">
    <a href="{{ route('inner_service', ['url' => $service->url]) }}">
        <div class="{{ $class }}-content services">
            <h1>{{ $service->name }}</h1>
            {{--<p>{{ $service->main_content }}</p>--}}
        </div>
    </a>
</div>
