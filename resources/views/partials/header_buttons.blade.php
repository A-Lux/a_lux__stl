
<li class="nav-item">
   

    @if($_SERVER["REQUEST_URI"] != "/")
        <a href="/#counter">
            <button class="head-btn">@lang('buttons.Рассчитать')</button>
        </a>
    @else
        <a href="#counter" class="anchor">
            <button class="head-btn" onclick="showCalculateForm()" >@lang('buttons.Рассчитать')</button>
        </a>
    @endif


</li>
<li class="nav-item">
    <a href="{{ Voyager::image(json_decode(setting('site.brochure'))[0]->download_link) }}" target="_blank">
        <button class="head-btn">@lang('buttons.Скачать брошюру')</button>
    </a>
</li>
<li class="nav-item">
    <a href="{{ setting('site.presentation') }}" target="_blank">
        <button class="head-btn">@lang('buttons.презентация')</button>
    </a>
</li>

