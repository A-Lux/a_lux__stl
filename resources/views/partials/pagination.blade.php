@if ($model->hasPages())
    <div class="review-item">
        <div class="row">
            <div class="col-xl-6">
                <div class="portfolio-btn">
                    <div class="pages-inner">
                        @if($model->previousPageUrl() != null)
                            <a href="{{ $model->previousPageUrl() }}" class="prevAndNext">@lang('buttons.Назад')  </a>
                        @else
                            <a class="prevAndNext"> @lang('buttons.Назад') </a>
                        @endif

                        @for($m = 1; $m <= $count; $m++)
                            <div class="pages" onchange='setLocation($(this).attr("data-url"))' data-url="{{ $model->url($m) }}">
                                <input type="radio" id="radio-page-{{ $m }}" name="radios-one"
                                        {{ $model->currentPage() == $m ? "checked" : ""}}>
                                <label for="radio-page-{{ $m }}">{{ $m }}</label>
                            </div>
                        @endfor
                        @if($model->nextPageUrl() != null)
                            <a href="{{ $model->nextPageUrl() }}" class="prevAndNext">@lang('buttons.Вперед')  </a>
                        @else
                            <a class="prevAndNext"> @lang('buttons.Вперед') </a>
                        @endif
                    </div>
                </div>
            </div>
            <div class="col-xl-6">
                <div class="all-review">
                    <span>@lang('texts.Всего :count Отзывов', ['count' => $count])  </span>
                </div>
            </div>
        </div>
    </div>
@endif
