<div class="container">
    <div class="head-content">
        <div class="row">
            <div class="col-xl-1">
                <div class="logo">
                    <a href="{{ route('home') }}">
                        <img src="{{ Voyager::image(setting('site.logo')) }}" alt="">
                    </a>
                </div>
            </div>
            <div class="col-xl-11">
                <div class="col-xl-12 top-head">
                    <div class="top-header">
                        <ul class="nav">
                            @include('/partials/header_contacts')
                            @include('/partials/header_buttons')
                        </ul>
                    </div>
                </div>
                <div class="col-xl-12">
                    <div class="bottom-header">
                        <ul class="navbar-navs">
                            @foreach (menu('site_header', '_json') as $menuItem)
                            @php $menuItem = $menuItem->translate(app()->getLocale());@endphp
                            @if(count($menuItem->children) == 0)
                            <li class="nav-item">
                                <a class="nav-link item icons" style="color: #fff" href="{{ $menuItem->url }}">{{ $menuItem->title}}</a>
                            </li>
                            @else
                            <li class="nav-item">
                                <button class="btn company-btn dropdown-toggle" type="button" id="dropdownMenuButton-company" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ $menuItem->title }} <img src="{{ asset('icons/chevron-down.svg') }}" class="icons">
                                </button>
                                <div class="dropdown-menu company-menu" aria-labelledby="dropdownMenuButton-company">
                                    @foreach($menuItem->children as $menuSubItem)
                                    @php $menuSubItem = $menuSubItem->translate(app()->getLocale());@endphp
                                    <a class="dropdown-item company-item" href="{{ $menuSubItem->url }}">{{ $menuSubItem->title }}</a>
                                    @endforeach
                                </div>
                            </li>
                            @endif
                            @endforeach
                            <form class="nav-item input-search" method="get" action="/search">

                                <input name="text" placeholder="@lang('texts.Поиск')" class="search-input" required>
                                <img class="search-img" src="{{ asset('icons/search%2024px.svg') }}" alt="">
                            </form>

                            <div class="dropdown skills">
                                <a class="btn dropdown-toggle skills-btn" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    {{ app()->isLocale("ru") ? ' RUS' : 'ENG' }} <img src="{{ asset('images/drop-arrow.png') }}" alt="">
                                </a>

                                <div class="dropdown-menu lang" aria-labelledby="dropdownMenuLink">
                                    <a class="dropdown-item head-btn" href="{{ route('lang', ['url' => app()->isLocale('ru') ? 'en' : 'ru']) }}">{{ app()->isLocale("ru") ? 'ENG' : 'RUS' }}</a>
                                </div>
                            </div>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="burger-menu">
    <a href="{{ route('home') }}">
        <img src="{{ Voyager::image(setting('site.logo')) }}" alt="">
    </a>
    <form class="nav-item input-search" method="get" action="/search">
        {{csrf_field()}}
        <input name="text" placeholder="@lang('texts.Поиск')" class="search-input" required>
        <button> <img class="search-img" src="{{ asset('icons/search%2024px.svg') }}" alt=""></button>
    </form>
    <a href="#" class="burger-menu-btn">
        <span class="burger-menu-lines"></span>
    </a>
</div>
<div class="nav-panel-mobil">
    <div class="dropdown skills mobile-lang">
        <a class="btn dropdown-toggle skills-btn" href="#" role="button" id="dropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{ app()->isLocale("ru") ? ' RUS' : 'ENG' }} <img src="{{ asset('images/drop-arrow.png') }}" alt="">
        </a>

        <div class="dropdown-menu lang" aria-labelledby="dropdownMenuLink">
            <a class="dropdown-item head-btn" href="{{ route('lang', ['url' => app()->isLocale('ru') ? 'en' : 'ru']) }}">{{ app()->isLocale("ru") ? 'ENG' : 'RUS' }}</a>
        </div>
    </div>
    <div class="container">
        <nav class="navbar-expand-lg navbar-light">
            <ul class="navbar-nav nav-mobile navbar-mobile d-flex flex-column">
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('home') }}">@lang('texts.Главная')</a>
                </li>
                @php $m = 0; @endphp
                @foreach (menu('site_header', '_json') as $menuItem)
                @php $m++; @endphp
                @php $menuItem = $menuItem->translate(app()->getLocale()) @endphp
                <li class="nav-item">
                    <a class="nav-link {{ $m > 2 ? "item icons" : "" }}" href="{{ $menuItem->url }}">{{ $menuItem->title }}</a>
                </li>
                @endforeach

                @include('/partials/header_buttons')
                @include('/partials/header_contacts', ['class' => 'nav-link'])

            </ul>

        </nav>
    </div>
</div>
