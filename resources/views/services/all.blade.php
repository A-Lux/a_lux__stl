@extends('layouts.app')
@section('content')

    @include('/partials/breadcrumbs')

    @foreach($services as $k => $v)
        @php $images = $v->images @endphp
        <div class="services {{ $k == count($services) ? 'services-bottom' : '' }}">
            <div class="container">
                <div class="row">
                    <div class="col-xl-5">
                        <div class="services-images">
                            <div class="slider-product">
                                <div class="slider-base_{{ $v->url }}">
                                    @foreach($images as $k1 => $v1)
                                        <div class="slides">
                                            <img src="{{ Voyager::image($v1->image) }}" alt="" style="width: 100%;height: 100%;">
                                        </div>
                                    @endforeach
                                </div>
                                <!-- <div class="slider-list_{{ $v->url }}" style="position: relative;display: block;overflow: hidden;margin: 0;padding: 0;">
                                    @foreach($images as $k1 => $v1)
                                        <div class="slider-links" style="width: 100%;">
                                            <img src="{{ Voyager::image($v1->image) }}" alt="" style="height: 85px;width: 140px;"></div>
                                    @endforeach
                                </div> -->
                            </div>
                            <div class="services-button">
                                <a href="/?service={{ $v->id }}" >
                                    <button class="head-btn">@lang('buttons.Рассчитать стоимость')</button>
                                </a>
                            </div>
                        </div>
                    </div>
                    <div class="col-xl-7" id="{{ $v->url }}_service">
                        <div class="service-content">
                            {!! $v->service_content !!}
                        </div>
                        <div class="read-overflow">
                            <a href="{{ route('inner_service', ['url' => $v->url]) }}">@lang('buttons.Читать далее')<img src="{{ asset('images/arrow-read.png') }}" alt=""></a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    @endforeach


@endsection
