@extends('layouts.app')
@section('content')

@include('/partials/breadcrumbs')

<div class="gallery-content">
    <div class="container">
        <div class="row">
            @foreach($model as $v)
            @php $gallery = $v->transportationExamples @endphp
            @if(count($gallery) > 0)
            <div class="col-xl-4 p-0 col-md-6 col-lg-4 wow zoomInUp">
                <div class="gallery-slider ">
                    @foreach($gallery as $val)
                    <div class="slide-product parent-container ">
                        <a href="{{ Voyager::image($val->image) }}"><img src="{{ Voyager::image($val->image) }}" style="width:387px;height: 284px;">
                            <h1>{{ $val->translate(app()->getLocale())->title }}</h1>
                            <p>{{ $val->translate(app()->getLocale())->description }}</p>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
            @endif
            @endforeach


            <div class="col-xl-4 p-0 col-md-6 col-lg-4">
                <div class="gallery-image">
                    <img src="{{ asset('images/service6.png') }}" alt="">
                </div>
            </div>
        </div>
    </div>
</div>

@endsection
