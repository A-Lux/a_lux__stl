@extends('layouts.app')
@section('content')

    <div class="service-title">
        @include('/partials/breadcrumbs')
    </div>
    <div class="service-page">
        <div class="container">
            <div class="transport-title wow bounceInRight">
                <h1>{{ $model->name }}</h1>
            </div>

            <div class="transport-slider wow bounceInLeft">
                @php $images = $model->images @endphp
                @if($images != null)
                    @foreach($images as $k => $v)
                        <div class="slider">
                            <img src="{{ Voyager::image($v->image) }}" alt="">
                            @if($v->description != null)
                                <div class="slider-txt" >
                                    <p>{{ $v->description }}</p>
                                </div>
                            @endif
                        </div>
                    @endforeach
                @endif
            </div>

            @if($model->inner_first_block != null)
                <div class="transport-content  wow bounceInUp" id="inner_first_block">
                    {!! $model->inner_first_block !!}
                </div>
                <div class="line"></div>
            @endif
            @if($model->inner_second_block != null)
                <div class="benefit  wow bounceInLeft" id="inner_second_block">
                    {!! $model->inner_second_block !!}
                </div>
            @endif

            @if($model->inner_third_block_status == 1)

                <div class="check-txt">
                    <div class="container">
                      {!! $model->inner_third_block_description !!}
                    </div>

                </div>
                <div class="check-link" id="inner_third_block_content">
                    <div class="container">
                        <div class="row">
                            <div class="col-xl-4 col-md-4 col-lg-4">
                                <div class="check-link-img">
                                    <img src="{{ Voyager::image($model->inner_third_block_image) }}" alt="">
                                </div>
                            </div>
                            <div class="col-xl-8 col-md-8 col-lg-8">
                                <div class="check-link-txt">
                                    {!! $model->inner_third_block_content !!}
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            @endif

        </div>
    </div>

@endsection
