<section class="footer">
    <div class="container">
        <div class="row justify-content-between">
            <div class="col-12 col-xs-6 col-sm-6 col-md-3 col-lg-3 foot ">
                <a href="#"><img src="{{ Voyager::image(setting('site.footer_logo')) }}" class='footer-img'></a>
                @foreach (menu('site_footer', '_json') as $menuItem)
                @php $menuItem = $menuItem->translate(app()->getLocale()) @endphp
                <a href="{{ $menuItem->url }}" style="text-decoration: none">{{ $menuItem->title }}</a>
                @endforeach
            </div>
            <div class="col-12 col-xs-6 col-sm-6 col-md-3 col-lg-3 foot">
                <h2>@lang('texts.Наши услуги') </h2>
                @foreach($services as $k => $v)
                <a href="{{ route('inner_service', ['url' => $v->url]) }}" style="text-decoration: none">{{ $v->name }}</a>
                @endforeach
            </div>
            <div class="col-12 col-xs-6 col-sm-6 col-md-3 col-lg-3 foot">
                <h2>@lang('texts.Разделы') </h2>
                @foreach (menu('site_footer_sections', '_json') as $menuItem)
                @php $menuItem = $menuItem->translate(app()->getLocale()) @endphp
                <a href="{{ $menuItem->url }}" style="text-decoration: none">{{ $menuItem->title }}</a>
                @endforeach
            </div>
            <div class="col-12 col-xs-6 col-sm-6 col-md-3 col-lg-3 foot">
                <h2>@lang('texts.Контакты') </h2>
                <p>@lang('texts.Адрес'): {{ $address->name }}</p>
                <p><a href="tel:{{ setting('site.telephone') }}">
                        {{ setting('site.telephone') }}</a></p>
                <p><a href="tel:{{ setting('site.telephone2') }}">
                        {{ setting('site.telephone2') }}</a></p>
                <p><a href="mailto:{{ setting('site.email') }}">
                        {{ setting('site.email') }}</a></p>
            </div>
        </div>
    </div>
</section>
<section class="bottom-footer">
    <div class="container">
        <div class="bottom-foot-content">
            <p>{{ setting('site.copyright') }}</p>
            <p>Разработано в<a href="#"><img src="{{ asset('images/footer-logo.png') }}" alt=""></a></p>
        </div>
        
    </div>
</section>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.4.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/popper.js@1.16.0/dist/umd/popper.min.js" integrity="sha384-Q6E9RHvbIyZFJoft+2mJbHaEWldlvI9IOYy5n3zV9zzTtmI3UksdQRVvoxMfooAo" crossorigin="anonymous"></script>
<script src="{{ asset('js/bootstrap.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('slick/slick.js') }}"></script>
<script type="text/javascript" src="{{ asset('slick/slick.min.js') }}"></script>
<script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/magnific-popup.js/1.1.0/jquery.magnific-popup.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/select2@4.0.13/dist/js/select2.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/wow/1.1.2/wow.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.maskedinput/1.4.1/jquery.maskedinput.min.js"></script>
<script src="{{ asset('js/main.js') }}"></script>
<script src="{{ asset('js/ajax.js') }}"></script>

<script>
    @foreach($services as $k => $v)
    $('.slider-base_{{ $v->url }}').slick({
        slidesToShow: 1,
        slidesToScroll: 1,
        arrows: false,
        fade: true,
        dots: false,
        asNavFor: '.slider-list_{{ $v->url }}'
    });
    $('.slider-list_{{ $v->url }}').slick({
        slidesToShow: 3,
        slidesToScroll: 1,
        asNavFor: '.slider-base_{{ $v->url }}',
        dots: false,
        centerMode: true,
        focusOnSelect: true
    });
    @endforeach
</script>


</body>

</html>