
<div class="modal fade" id="consultationModal" tabindex="-1" role="dialog" aria-labelledby="consultationModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('buttons.Получить консультацию')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group">

                    <label for="recipient-name" class="col-form-label">@lang('texts.Ваше имя'):</label>
                    <input type="text" class="form-control" id="name" name="name">

                    <label for="recipient-name" class="col-form-label">@lang('texts.Ваше e-mail'):</label>
                    <input type="text" class="form-control" id="name" name="email">

                    <label for="message-text" class="col-form-label">@lang('texts.Ваш телефон'):</label>
                    <input class="form-control telephone" id="telephone" name="telephone" placeholder="@lang('texts.Например'): +77087772757">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" id="consultationBtn" style="background: #191A27;border-radius: 100px;color: #FFFFFF;">@lang('texts.Жду звонка')</button>
            </div>
        </form>
    </div>
</div>







<div class="modal fade" id="feedbackModal" tabindex="-1" role="dialog" aria-labelledby="feedbackModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
        <form class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title">@lang('buttons.Заказать обратный звонок')</h5>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">

                <div class="form-group">

                    <label for="recipient-name" class="col-form-label">@lang('texts.Ваше имя'):</label>
                    <input type="text" class="form-control" id="name" name="name">

                    <label for="recipient-name" class="col-form-label">@lang('texts.Ваше e-mail'):</label>
                    <input type="text" class="form-control" id="name" name="email">

                    <label for="message-text" class="col-form-label">@lang('texts.Ваш телефон'):</label>
                    <input class="form-control telephone" id="telephone" name="telephone" placeholder="@lang('texts.Например'): +77087772757">
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn" id="feedbackBtn" style="background: #191A27;border-radius: 100px;color: #FFFFFF;">@lang('texts.Жду звонка')</button>
            </div>
        </form>
    </div>
</div>

