<section class="head">
    <div class="video">
        @php $file = json_decode($banner->image)[0]->download_link; @endphp
        <video id="vid" src="{{ Voyager::image($file) }}" autoplay playsinline width="100%" muted='muted' loop="loop">
            <source src="{{ Voyager::image($file) }}" type='video/mp4; codecs="avc1.42E01E, mp4a.40.2"'>
        </video>
        <script>
            $('#vid')[0].play()
        </script>
    </div>

    <div class="header">
        @include('/partials/header_menu')
    </div>
    <div class="header-content wow bounceInLeft">
        <div class="container">
            <h1 style="color: #fff">{{ $banner->title }}</h1>
        </div>
    </div>
</section>
