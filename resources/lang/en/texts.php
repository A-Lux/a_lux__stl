<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Pagination Language Lines
    |--------------------------------------------------------------------------
    |
    | The following language lines are used by the paginator library to build
    | the simple pagination links. You are free to change them to anything
    | you want to customize your views to better match your application.
    |Адрес
    */

    'Мобильный'                                             => 'Mobile',
    'Филиалы компании'                                      => 'Company branches',
    'Откуда'                                                => 'From',
    'Куда'                                                  => 'To',
    'Описание (вес, объем, размер)'                         => 'Description (weight, volume, size)',
    'Телефон'                                               => 'Telephone',
    'Ваш email'                                             => 'Your email',
    'Наши услуги'                                           => 'Our services',
    'Разделы'                                               => 'Sections',
    'Контакты'                                              => 'Contacts',
    'Адрес'                                                 => 'Address',
    'Главная'                                               => 'Home',
    'Услуги'                                                => 'Services',
    'Всего :count Отзывов'                                  => 'Total :count Reviews',
    'Страница не найдена'                                   => 'Page not found',
    'Извините, страница, которую вы ищете, не найдена'      => 'Sorry, the page you are looking for was not found.',
    'Ваше имя'                                              => 'Your name',
    'Ваше e-mail'                                           => 'Your email',
    'Жду звонка'                                            => 'Waiting for a call',
    'Например'                                              => 'For example',
    'Ваш телефон'                                           => 'Your phone number',
    'Заказать обратный звонок'                              => 'Request a call back',
    'Результаты по запросу'                                 => 'Results for request',
    'По запросу'                                            => 'For request',
    'ничего не найдено'                                     => 'nothing found.',
    'О компании'                                            => 'About company',
    'Отзывы'                                                => 'Reviews',
    'Поиск'                                                 => 'Search',
    'success_title'                                         => 'Request sent successfully!',
    'success_content'                                       => 'In the near future we will contact you.',

];
