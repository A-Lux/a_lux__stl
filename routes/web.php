<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'HomeController@index')->name('home');
Route::get('/about', 'AboutController@index')->name('about');
Route::get('/contacts', 'ContactController@index')->name('contact');

Route::get('/services', 'ServiceController@all')->name('all_service');
Route::get('/service/{url}', 'ServiceController@index')->name('inner_service');
Route::get('/galleries', 'ServiceController@gallery')->name('galleries');

Route::get('/reviews', 'ReviewController@index')->name('reviews');
Route::get('/lang/{url}', 'LangController@index')->name('lang');
Route::get('/search', 'SearchController@index')->name('search');

Route::post('/service-request', 'RequestController@saveService');
Route::post('/feedback-request', 'RequestController@saveFeedback');
Route::post('/consultation-request', 'RequestController@saveConsultation');

Route::group(['prefix' => 'admin'], function () {
    Voyager::routes();
});
