<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 27.01.2020
 * Time: 17:26
 */

namespace App;


use Illuminate\Database\Eloquent\Model;

class Feedback extends Model
{
    protected $fillable = ['name', 'email', 'telephone'];

}
