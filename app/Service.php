<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Service extends Model
{
    use Translatable;
    protected $translatable = ['name','service_content','service_description','inner_first_block',
        'inner_second_block', 'meta_title', 'meta_description', 'meta_keyword','inner_third_block_description', 'inner_third_block_content'];


    public static function getByUrl($url){
        return Service::where('url', $url)->with("transportationExamples", "images")->first();
    }


    public static function getAllWithTransportationExamples(){
        return Service::with("transportationExamples")->get();
    }

    public static function getAllWithImages(){
        return Service::with("images")->get();
    }

    public function transportationExamples() {
        return $this->hasMany(ServiceTransportationExample::class)->orderBy('sort', 'ASC');
    }

    public function images() {
        return $this->hasMany(ServiceImage::class)->orderBy('sort', 'ASC');
    }

    public static function search($text){
        if(app()->getLocale() == 'ru') $model = self::where('service_content', 'LIKE', "%{$text}%")->get();
        else $model = self::whereTranslation('service_content', 'LIKE', "%{$text}%", [app()->getLocale()], false)->get();
        return $model;
    }

    public static function searchByInnerFirstBlock($text){
        if(app()->getLocale() == 'ru') $model = self::where('inner_first_block', 'LIKE', "%{$text}%")->get();
        else $model = self::whereTranslation('inner_first_block', 'LIKE', "%{$text}%", [app()->getLocale()], false)->get();
        return $model;
    }

    public static function searchByInnerSecondBlock($text){
        if(app()->getLocale() == 'ru') $model = self::where('inner_second_block', 'LIKE', "%{$text}%")->get();
        else $model = self::whereTranslation('inner_second_block', 'LIKE', "%{$text}%", [app()->getLocale()], false)->get();
        return $model;
    }

    public static function searchByInnerThirdBlock($text){
        if(app()->getLocale() == 'ru') $model = self::where('inner_third_block_content', 'LIKE', "%{$text}%")->get();
        else $model = self::whereTranslation('inner_third_block_content', 'LIKE', "%{$text}%", [app()->getLocale()], false)->get();
        return $model;
    }

    
}
