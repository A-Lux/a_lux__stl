<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class AboutReview extends Model
{
    use Translatable;
    protected $translatable = ['company', 'name', 'content'];
    public $url;

    public static function getCount($per_page){
        $quantity = self::orderBy("sort", "ASC")->count();
        if($quantity % $per_page != 0) $last = 1;
        else $last = 0;
        return $quantity / $per_page + $last;
    }

    public static function getAllByStatus(){
        return self::where('status', 1)->orderBy("sort", "ASC")->get();
    }


    public static function getAllWithPagination($perPage){
        $model = self::orderBy("sort", "ASC")->paginate($perPage);
        $model->getCollection()->transform(function($model) {
            return $model->translate(app()->getLocale());
        });

        return $model;
    }

    public static function search($text, $perPage){
        $pagination = self::get();
        if(app()->getLocale() == 'ru') $model = self::where('company', 'LIKE', "%{$text}%")
            ->orWhere('name', 'LIKE', "%{$text}%")
            ->orWhere('content', 'LIKE', "%{$text}%")->get();
        else $model = self::whereTranslation('content', 'LIKE', "%{$text}%", [app()->getLocale()], false)->get();
       foreach ($model as $k => $v){
           $m = 0;
           foreach ($pagination as $v2){
               $m++;
               if($v->id == $v2->id){
                   $v->url = 'reviews?page='.((int)($m / $perPage) + ($m % $perPage != 0 ? 1 : 0)).'#review_'.$v->id;
               }
           }
       }
       return $model;
    }


}
