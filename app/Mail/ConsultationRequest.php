<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;
use App\Consultation as RequestForConsultation;

class ConsultationRequest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var RequestForConsultation
     */
    public $model;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(RequestForConsultation $request)
    {
        $this->model = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
        {
        return $this->view('mails.consultation')->subject("Пользователь хочет получить консультацию");
    }
}
