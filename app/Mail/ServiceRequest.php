<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 23.01.2020
 * Time: 17:05
 */

namespace App\Mail;


use App\Request as RequestForService;
use Illuminate\Bus\Queueable;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class ServiceRequest extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * The order instance.
     *
     * @var RequestForService
     */
    public $model;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct(RequestForService $request)
    {
        $this->model = $request;
    }

    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->view('mails.service')->subject("Пользователь хочет рассчитать стоимость");
    }

}
