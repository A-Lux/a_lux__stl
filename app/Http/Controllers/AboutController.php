<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 18.02.2020
 * Time: 16:33
 */

namespace App\Http\Controllers;


use App\About;
use App\AboutFindOutMore;
use App\AboutReview;
use App\AboutTitle;
use App\AboutWhyOurCompany;
use App\Certification;
use App\Helpers\TranslatesCollection;
use App\Partner;

class AboutController extends Controller
{
    public function index(){

        $head = AboutTitle::getAll();
        $model = About::getContent();
        $whyOurCompany = AboutWhyOurCompany::getAll();
        $findOutMore = AboutFindOutMore::getContent();
        $reviews = AboutReview::getAllByStatus();
        $partners = Partner::getAll();
        $certificates = Certification::getAll();

        TranslatesCollection::translate($head, app()->getLocale());
        TranslatesCollection::translate($model, app()->getLocale());
        TranslatesCollection::translate($whyOurCompany, app()->getLocale());
        TranslatesCollection::translate($findOutMore, app()->getLocale());
        TranslatesCollection::translate($reviews, app()->getLocale());


        return view('about.index', compact('head', 'model', 'whyOurCompany', 'findOutMore',
            'reviews', 'partners', 'certificates'));
    }

}
