<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 18.02.2020
 * Time: 17:16
 */

namespace App\Http\Controllers;


use App\Helpers\TranslatesCollection;
use App\Service;
use App\ServiceImage;

class ServiceController extends Controller
{
    public function index($url){

        $model = Service::getByUrl($url);
        if($model){
            TranslatesCollection::translate($model, app()->getLocale());
            return view('services.index', compact('model'));
        }else{
            abort(404);
        }
    }

    public function all(){

        return view('services.all');
    }


    public function gallery(){

        $model = Service::getAllWithTransportationExamples();

        return view('services.gallery', compact('model'));
    }


}
