<?php

namespace App\Http\Controllers;


use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use App\Request as RequestForService;
use App\Feedback as RequestForFeedback;
use App\Consultation as RequestForConsultation;
use App\Mail\FeedbackRequest;
use App\Mail\ServiceRequest;
use App\Mail\ConsultationRequest;

class RequestController extends Controller
{

    const messageSuccessTitle = 'texts.success_title';
    const messageSuccessContent = 'texts.success_content';

    public function saveService(Request $request) {

        $validator = Validator::make($request->all(), [
            'cargo_from'   => 'required',
            'cargo_to'     => 'required',
            'description'  => 'required',
            'email'        => 'required|email',
            'telephone'    => 'required|regex:/(\+77)[0-9]{9}/',
            'service_id'   => 'required',
        ]);


        if ($validator->fails()) {
            $errors = $validator->errors();
            return response(['status' => 0,'error' => $errors->first()], 200);
        }else {
            $model = RequestForService::create($request->all());
            Mail::to(setting('site.email'))->send(new ServiceRequest($model));
            return response(['status' => 1, 'title' => trans(self::messageSuccessTitle),
                'content' => trans(self::messageSuccessContent)], 200);
        }

    }


    public function saveFeedback(Request $request) {
        $validator = Validator::make($request->all(), [
            'name'        => 'required',
            'email'       => 'required|email',
            'telephone'   => 'required|regex:/(\+77)[0-9]{9}/',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response(['status' => 0,'error' => $errors->first()], 200);
        }else{
            $model = RequestForFeedback::create($request->all());
            Mail::to(setting('site.email'))->send(new FeedbackRequest($model));
            return response(['status' => 1, 'title' => trans(self::messageSuccessTitle),
                'content' => trans(self::messageSuccessContent)], 200);
        }
    }

    public function saveConsultation(Request $request) {
        $validator = Validator::make($request->all(), [
            'name'        => 'required',
            'email'       => 'required|email',
            'telephone'   => 'required|regex:/(\+77)[0-9]{9}/',
        ]);

        if ($validator->fails()) {
            $errors = $validator->errors();
            return response(['status' => 0,'error' => $errors->first()], 200);
        }else{
            $model = RequestForConsultation::create($request->all());
            Mail::to(setting('site.email'))->send(new ConsultationRequest($model));
            return response(['status' => 1, 'title' => trans(self::messageSuccessTitle),
                'content' => trans(self::messageSuccessContent)], 200);
        }
    }



}
