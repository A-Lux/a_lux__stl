<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 02.03.2020
 * Time: 18:02
 */

namespace App\Http\Controllers;


use App\About;
use App\AboutFindOutMore;
use App\AboutReview;
use App\AboutWhyOurCompany;
use App\Address;
use App\Branch;
use App\Helpers\TranslatesCollection;
use App\Service;
use Illuminate\Http\Request;

class SearchController extends Controller
{


    public function index(Request $request){

        $text = $request->input('text');

        $about = About::search($text);
        $whyOurCompany = AboutWhyOurCompany::search($text);
        $findOutMore = AboutFindOutMore::search($text);
        $service_content = Service::search($text);
        $services_first = Service::searchByInnerFirstBlock($text);
        $services_second = Service::searchByInnerSecondBlock($text);
        $services_third = Service::searchByInnerThirdBlock($text);
        $reviews = AboutReview::search($text, $this->paginationPerPage);
        $addresss = Address::search($text);
        $filials = Branch::search($text);


        if($about || $whyOurCompany->count() > 0 || $findOutMore || $service_content->count() > 0 ||
            $services_first->count() > 0 || $services_second->count() > 0 || $services_third->count() > 0 ||
            $reviews->count() > 0 || $addresss || $filials){
            if($about) TranslatesCollection::translate($about, app()->getLocale());
            if($whyOurCompany) TranslatesCollection::translate($whyOurCompany, app()->getLocale());
            if($findOutMore) TranslatesCollection::translate($findOutMore, app()->getLocale());
            if($service_content) TranslatesCollection::translate($service_content, app()->getLocale());
            if($services_first) TranslatesCollection::translate($services_first, app()->getLocale());
            if($services_second) TranslatesCollection::translate($services_second, app()->getLocale());
            if($services_third) TranslatesCollection::translate($services_third, app()->getLocale());
            if($reviews) TranslatesCollection::translate($reviews, app()->getLocale());
            if($addresss) TranslatesCollection::translate($addresss, app()->getLocale());
            if($filials) TranslatesCollection::translate($filials, app()->getLocale());

            $resultStatus = true;
        }else{
            $resultStatus = false;
        }

        return view('search.index', compact('resultStatus','text', 'about', 'whyOurCompany',
            'findOutMore', 'service_content', 'services_first', 'services_second', 'services_third', 'reviews',
            'addresss', 'filials'));
    }





}
