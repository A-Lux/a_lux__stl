<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 27.01.2020
 * Time: 11:43
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class AboutFindOutMoreController extends VoyagerBaseController
{
    public function index(Request $request){
        return parent::show($request, 1);
    }

}
