<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 27.01.2020
 * Time: 9:44
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class AboutController extends VoyagerBaseController
{
    public function index(Request $request){
        return parent::show($request, 1);
    }
}
