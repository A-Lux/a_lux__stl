<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 23.01.2020
 * Time: 16:02
 */

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use TCG\Voyager\Http\Controllers\VoyagerBaseController;

class AddressController extends VoyagerBaseController
{
    public function index(Request $request){
        return parent::show($request, 1);
    }

}
