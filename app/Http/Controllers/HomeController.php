<?php
/**
 * Created by PhpStorm.
 * User: admin
 * Date: 18.02.2020
 * Time: 15:34
 */

namespace App\Http\Controllers;


use App\Banner;
use App\Benefit;
use App\Helpers\TranslatesCollection;
use App\MainTitle;
use App\Partner;
use App\Service;

class HomeController extends Controller
{
    public function index(){

        $head = MainTitle::getAll();
        $benefits = Benefit::getAll();
        $partners = Partner::getAllOnMain();

//        die(app()->getLocale());
        TranslatesCollection::translate($head, app()->getLocale());
        TranslatesCollection::translate($benefits, app()->getLocale());

        return view('home.index', compact('head', 'benefits', 'partners'));
    }
}
