<?php

namespace App\Providers;

use App\Address;
use App\Banner;
use App\Helpers\TranslatesCollection;
use App\Page\Page;
use App\Service;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class ViewServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {

    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot(Page $page, Service $services, Banner $banner, Address $address)
    {

        View::composer('*', function($view) use($page) {
            $view->with(['page' => $page]);
        });


        View::composer('*', function($view) use($services) {
            $services = $services::getAllWithImages();
            TranslatesCollection::translate($services, app()->getLocale());
            $view->with(['services' => $services]);
        });


        View::composer('*', function($view) use($banner) {
            $banner = $banner::getContent();
            TranslatesCollection::translate($banner, app()->getLocale());
            $view->with(['banner' => $banner]);
        });

        View::composer('*', function($view) use($address) {
            $address = $address::getContent();
            TranslatesCollection::translate($address, app()->getLocale());
            $view->with(['address' => $address]);
        });

    }
}
