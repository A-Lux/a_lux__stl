<?php

namespace App\Providers;

use App\Helpers\TranslatesCollection;
use App\Page\Page;
use App\Page as PageModel;
use App\Service as ServiceModel;
use App\Meta\MetaTag;
use Illuminate\Support\Facades\Request;
use Illuminate\Support\ServiceProvider;

class MetaTagsProvider extends ServiceProvider
{
    /**
     * Register services.
     *
     * @return void
     */


    protected $page;

    public function register()
    {
        $this->app->singleton(MetaTag::class, function ($app) {
            return new MetaTag(config('app.meta'));
        });

        $this->app->singleton(Page::class, function ($app) {
            return new Page();
        });
    }

    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot(MetaTag $meta, Page $page)
    {
        $pageInstance = PageModel::getByUrl(Request::segment(1));
        $serviceInstance = ServiceModel::getByUrl(Request::segment(2));

        if($serviceInstance){
            TranslatesCollection::translate($serviceInstance, app()->getLocale());
            $page->setPage($serviceInstance);
            if($serviceInstance->meta_title) {
                $meta->setTitle($serviceInstance->meta_title);
                $meta->setDescription($serviceInstance->meta_description);
                $meta->setKeyword($serviceInstance->meta_keyword);
            }
        }else if($pageInstance) {
            TranslatesCollection::translate($pageInstance, app()->getLocale());
            $page->setPage($pageInstance);
            if($pageInstance->meta_title) {
                $meta->setTitle($pageInstance->meta_title);
                $meta->setDescription($pageInstance->meta_description);
                $meta->setKeyword($pageInstance->meta_keyword);
            }
        }

        $page->setMeta($meta);
    }
}
