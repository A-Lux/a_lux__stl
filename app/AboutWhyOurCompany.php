<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class AboutWhyOurCompany extends Model
{
    use Translatable;
    protected $translatable = ['title', 'content'];

    public static function getAll(){
        return AboutWhyOurCompany::orderBy("sort", "ASC")->get();
    }

    public static function search($text){
        if(app()->getLocale() == 'ru') $model = self::where('content', 'LIKE', "%{$text}%")->get();
        else $model = self::whereTranslation('content', 'LIKE', "%{$text}%", [app()->getLocale()], false)->get();
        return $model;
    }
}
