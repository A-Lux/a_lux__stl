<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class Branch extends Model
{
    use Translatable;
    protected $translatable = ['country', 'address'];

    public static function getAll(){
        return Branch::orderBy('sort', "ASC")->get();
    }

    public static function search($text){
        if(app()->getLocale() == 'ru') $model = self::where('country', 'LIKE', "%{$text}%")
            ->orWhere('address', 'LIKE', "%{$text}%")->get();
        else $model = self::whereTranslation('address', 'LIKE', "%{$text}%", [app()->getLocale()], false)->get();
        return $model;
    }
}
