<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class AboutFindOutMore extends Model
{
    use Translatable;
    protected $translatable = ['content'];

    public static function getContent(){
        return AboutFindOutMore::first();
    }

    public static function search($text){
        if(app()->getLocale() == 'ru') $model = self::where('content', 'LIKE', "%{$text}%")->first();
        else $model = self::whereTranslation('content', 'LIKE', "%{$text}%", [app()->getLocale()], false)->first();
        return $model;
    }
    
}
