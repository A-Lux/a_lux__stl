<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class About extends Model
{
    use Translatable;
    protected $translatable = ['first_block_con', 'second_block_title', 'second_block_con','third_block_title','third_block_co'];

    public static function getContent(){
        return self::first();
    }

    public static function search($text){
        if(app()->getLocale() == 'ru') $model = self::where('first_block_con', 'LIKE', "%{$text}%")->first();
        else $model = self::whereTranslation('first_block_con', 'LIKE', "%{$text}%", [app()->getLocale()], false)->first();
        return $model;
    }


}


