<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use TCG\Voyager\Traits\Translatable;


class AboutTitle extends Model
{
    use Translatable;
    protected $translatable = ['name'];

    public static function getAll(){
        return AboutTitle::get();
    }
    
}
