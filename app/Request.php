<?php

namespace App;

use Illuminate\Database\Eloquent\Model;


class Request extends Model
{
    protected $fillable = ['cargo_from', 'cargo_to', 'description', 'email', 'telephone', 'service_id'];
    public function service(){
        return $this->belongsTo(Service::class);
    }
}
