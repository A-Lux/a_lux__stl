<?php

namespace App\Helpers;


class TranslatesCollection {

    public static function translate(&$collection, $locale) {
        if(count($collection) > 1){
            foreach($collection as $key => $item) {
                $collection[$key] = $item->translate($locale);
            }
        }else{
            $collection = $collection->translate($locale);
        }
    }



}

